﻿===== Destiny Oriented Responsive Assistent Xplorer =====

CAPA [PEDRO]
RESUMO [PEDRO]
	// adicionar Áreas de Aplicação e Palavras-Chave
INDICES (Geral, Figuras e Tabelas) [PEDRO]
1. Introdução
	1.1. Contextualização [PEDRO & VITALIY]
		// Pesquisar Exemplos Reais (Semelhanças e Diferenças) [VITALIY]
			* GeoCashing
			* ...
	1.2. Apresentação do Caso de Estudo [PEDRO]
	1.3. Motivação e Objetivos [PEDRO & JP]
		// Referir a motivação com base no que foi falado na contextualização e juntar as funcionalidades referidas demonstrando a necessidade da aplicação
		// Funcionalidades da Aplicação [JP]
			* Assistente de campo (DORA)
				- Indicar percursos e atividades préviamente indicadas pelo utilizador
				- Estruturar em formato .XML nomes, descrições (Voz) e fotos dos pontos descobertos
			* Recurso a API's existentes (Camara, Reconhecimento de Voz e Mapas Bing)
			* Base de Dados
			* Sincronização do serviço com a BD
				- Existência de plataforma (Windows) que permite carregamento de mapas,
				  gerência de percursos, cronologia de atividades e perfil
				- Dispositivo móvel (Android) em modo OFFLINE
			* Perfis de utilizador
			* Sistemas de Pontos por Ranking (Gold, Silver, Bronze)
			* Sistemas Operativos (Windows e Android/iOS)
	1.4. Estrutura do Relatório [PEDRO]
2. Planeamento e Construção da Plataforma
	2.1 Visão Geral [PEDRO & ANDRÉ & JP]
	// Microsoft Office Project [ANDRÉ]
	// Diagramas de Atividade (Utilização da Plataforma) [JP]
	2.2 Esboço da Interface Gráfica[ANDRÉ]
	// Gráfica de Base (Microsoft Visio) - Template Spotify
4. Conclusões e Trabalho Futuro [PEDRO]
5. Referências [PEDRO]
6. Lista de Siglas e Acrónimos [PEDRO]
7. Anexos [PEDRO]
