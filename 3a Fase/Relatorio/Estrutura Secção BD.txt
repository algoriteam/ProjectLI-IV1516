*** ESTRUTURA PARA SECÇÃO DA BD ***

4.1. Caracterização dos Perfis de Utilização
    4.1.1. Administrador
    4.1.2. Utilizador
4.2. Modelo Conceptual
    4.2.1. Identificação de Tipos de Entidades
    4.2.2. Identificação de Tipos de Relacionamentos
    4.2.3. Descrição, Domínios e Associação de Atributos com Tipo de Entidades ou Relacionamentos
    4.2.4. Identificação de Chaves Candidatas e Primárias
    4.2.5. Diagrama Conceptual
    4.2.6. Verificação do Modelo por Redundância
        4.2.6.1 Relações 1:1
        4.2.6.2 Remover relações redundantes
    4.2.7. Validação do Modelo com Possível Transações de Utilizadores
        4.2.7.1 Descrição de transações
4.3. Modelo Lógico
    4.3.1. Ilustração Modelo Lógico
    4.3.2. Descrição de Cada Relacionamento
        4.3.2.1 Relacionamentos de 1 para 1 (1:1)
        4.3.2.2 Relacionamentos de 1 para Muitos (1:n)
        4.3.2.3 Relacionamentos de Muitos para Muitos (n:n)
    4.3.3. Validação das Relações recorrendo à Teoria da Normalização
        4.3.3.1 Primeira Forma Normal (1FN)
        4.3.3.2 Segunda Forma Normal (2FN)
        4.3.3.3 Terceira Forma Normal (3FN)
    4.3.4. Validação das Relações a partir das Transações
    4.3.5. Verificar Restrições de Integridade
    4.3.6. Verificar Modelo Lógico com o Utilizador
    4.3.7. Verificar Expansão Futura do Modelo
4.4. Modelo Físico
    4.4.1. Implementação e Conversão do Modelo Lógico para um SGBD
        4.4.1.1 Criação de Relações
        4.4.1.2 Relações Base
        4.4.1.3 Representação de Dados Derivados
        4.4.1.4 Restrições Gerais
        4.4.1.5 Triggers
    4.4.2. Analisar Transações
        4.4.2.1 Transações Críticas à Operacionalidade do Negócio
        4.4.2.2 Períodos de Maior Utilização da Base de Dados
        4.4.2.3 Mapa dos Caminhos Transacionais nas Relações
        4.4.2.4 Frequência de Informação
    4.4.3. Representação de Transações
    4.4.4. Escolha de Organização de Ficheiros
    4.4.5. Escolha de Índices
    4.4.6. Definição de Vistas de Utilizadores
    4.4.7. Política de Segurança
        4.4.7.1 Regras de Acesso
        4.4.7.2 Dados
    4.4.8. Povoamento da BD
    4.4.9. Estimar Uso de Disco para os Dados Referidos e Estimar Futuro Crescimento
    4.4.10. Validação de Transações com o Utilizador
