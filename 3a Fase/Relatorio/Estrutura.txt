===== Destiny Oriented Responsive Assistent Xplorer =====

CAPA [PEDRO]
RESUMO [PEDRO]
	// adicionar �reas de Aplica��o e Palavras-Chave
INDICES (Geral, Figuras e Tabelas) [PEDRO]
1. Introdu��o
	1.1. Contextualiza��o
	1.2. Apresenta��o do Caso de Estudo
	1.3. Motiva��o e Objetivos
	1.4. Estrutura do Relat�rio [PEDRO]
2. Planeamento e Constru��o da Plataforma
	2.1 Vis�o Geral
	2.2 Esbo�o da Interface Gr�fica
3. Levantamento e An�lise de Requisitos
	Requisitos Funcionais
	Requisitos N�o-Funcionais
4. Base de Dados do Sistema
	Modelo Conceptual
		* Tipos de Entidades
		* Tipos de Relacionamentos
		* Descri��o, Dom�nio e Associa��o dos atributos com tipo de entidades ou relacionamentos
		* Identifica��o de chaves candidatas e prim�rias (por tabela)
		* Diagrama de Chen
		* Verifica��o do Modelo por Redund�ncia
			- Rela��es 1:1
			- Remover rela��es redundantes
		* Valida��o do Modelo com poss�veis transa��es de utilizadores
			- Descri��o de transa��es
		Valida��o do Modelo segundo os requisitos levantados
		
	Modelo L�gico
		* Ilustra��o do Modelo L�gico
		* Identifica��o e Descri��o das Rela��es
		* Valida��o do Modelo recorrendo � Teoria da Normaliza��o
			(Ir a cada forma ou definir logo a forma em que est�)
		* Valida��o das rela��es a partir das Transa��es
		* Verifica��o das Restri��es de integridade
		* Verificar Modelo L�gico com o Utilizador/Cliente
		* Estimativa de Expans�o Futura do Modelo
		(Dicionario de Dados)-> onde � que isto fica mesmo?

	Modelo F�sico
		* Implementa��o e Convers�o do Modelo L�gico para um SGBD
			- Cria��o de Rela��es
			- Rela��es Base
			- Representa��o de Dados Derivados
			- Retri��es Gerais
			- Triggers
		* Analise de Transa��es
			- Transa��es Criticas � Operacionalidade do Neg�cio
			- Periodos de Maior Utiliza��o da Base de Dados
			- Mapa dos Caminhos Transacionais nas Rela��es
			- Frequ�ncia de Informa��o
		* Representa��o de Tranasa��es
		* Escolha de Organiza��o de Ficheiros
		* Escolha de Indices
		* Defini��o de Vistas de Utilizador
		* Pol�ticas de Seguran�a
			- Regras de Acesso (utilizadores e permiss�es)
			- Dados
		* Povoamento da BD
		* Estimar Uso de Disco para os Dados Referidos e Estimar Futuro 			Crescimento
		* Valida��o de Transa��es com o utilizador/cliente
		* Valida��o dos Requisitos Segundo as Funcionalidades propostas 			pelo docente/cliente
5. Modela��o do Sistema em UML
	Modelo Dominio
	Modelo de Use Cases
	Especifica��es Textual dos Use Case
	Diagrama de Sequ�ncia (Simples, Subsistemas ou Implementa��o)
	Diagrama de Classes
Escolha das tecnologias a utilizar
Defini��o da Stack de Prioridade dos requisitos a Implementar
4. Conclus�es e Trabalho Futuro [PEDRO]
5. Refer�ncias [PEDRO]
6. Lista de Siglas e Acr�nimos [PEDRO]
7. Anexos [PEDRO]