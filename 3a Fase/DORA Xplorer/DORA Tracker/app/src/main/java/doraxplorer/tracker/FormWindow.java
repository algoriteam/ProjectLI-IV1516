package doraxplorer.tracker;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.Image;
import android.media.MediaRecorder;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import business.Photo;

public class FormWindow extends Activity {

    private TextToSpeech DORA;
    private EditText name;
    private ImageView name_speech;
    private TextView description;
    private ImageView description_speech;
    private TextView point_number;

    private TextView num_photos;
    private int current_n_photos;
    private String current_point;

    private ImageView microphone;

    private static int RECORDER_SAMPLE = 8000;
    private static int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_DEFAULT;
    private static int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static int BUFFERSIZE;

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private boolean recording = false;
    private short[] full_recording;

    private ImageView take_picture;
    private List<Photo> photos;

    private Button ok_button;

    private String old_name, old_description, new_name, new_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_form_window);

        name = (EditText) findViewById(R.id.name);
        name_speech = (ImageView) findViewById(R.id.name_speech);
        description = (TextView) findViewById(R.id.description);
        description_speech = (ImageView) findViewById(R.id.description_speech);
        point_number = (TextView) findViewById(R.id.point_number);
        num_photos = (TextView) findViewById(R.id.num_photos);
        microphone = (ImageView) findViewById(R.id.microphone);
        take_picture = (ImageView) findViewById(R.id.camera);
        ok_button = (Button) findViewById(R.id.ok_button);

        photos = new ArrayList<>();

        Intent intent = getIntent();

        current_point = intent.getStringExtra("point_number");
        point_number.setText("Point number "+current_point, TextView.BufferType.EDITABLE);

        old_name = remove_spaces(intent.getStringExtra("name"));
        name.setText(old_name);

        old_description = remove_spaces(intent.getStringExtra("description"));
        description.setText(old_description);

        String u_photos = intent.getStringExtra("num_photos");
        current_n_photos = Integer.parseInt(u_photos);

        num_photos.setText(current_n_photos+"/3", TextView.BufferType.EDITABLE);

        DORA  = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    DORA.setLanguage(Locale.UK);
                    DORA.setSpeechRate(0.7f);
                    show_dialog("You reached point number "+current_point);
                }
                else{
                    show_dialog("Error starting DORA Assistant ...");
                }
            }
        });

        name_speech.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                name_speech.setImageResource(R.drawable.m_on_button);
                promptSpeechInput(2);
            }
        });

        description_speech.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                description_speech.setImageResource(R.drawable.m_on_button);
                promptSpeechInput(3);
            }
        });

        num_photos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try{
                    PlayAudio(full_recording);
                }catch(IOException e){
                    show_dialog("Something went wrong trying to play recorder file ...");
                }
            }
        });

        take_picture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(current_n_photos<3) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 1);
                    }
                }
                else{
                    show_dialog("Number limit of photos reached ...");
                }
            }
        });

        microphone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!recording){
                    BUFFERSIZE = AudioRecord.getMinBufferSize(RECORDER_SAMPLE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
                    microphone.setImageResource(R.drawable.m_on_button);

                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                            RECORDER_SAMPLE, RECORDER_CHANNELS,
                            RECORDER_AUDIO_ENCODING, BUFFERSIZE);

                    recorder.startRecording();
                    recording = true;
                    recordingThread = new Thread(new Runnable() {
                        public void run() {
                            writeAudioDataToFile();
                        }
                    });
                    recordingThread.start();
                }
                else {
                    microphone.setImageResource(R.drawable.m_off_button);
                    recording = false;
                    recorder.stop();
                    recorder.release();
                    recorder = null;
                    recordingThread = null;
                }
            }
        });

        ok_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new_name = remove_spaces(name.getText().toString());
                new_description = remove_spaces(description.getText().toString());
                if(!old_description.equals("") && new_description.equals("") && !old_name.equals("") && new_name.equals("")){
                    show_dialog("Name and description can\'t be blank now because old value isn't null ...");
                    name.setError("Error ...");
                    description.setError("Error ...");
                    name.setText(old_name);
                    description.setText(old_description);
                }
                else {
                    if (!old_description.equals("") && new_description.equals("")) {
                        show_dialog("Description can\'t be blank now because old value isn't null ...");
                        description.setError("Error ...");
                        description.setText(old_description);
                    } else {
                        if (!old_name.equals("") && new_name.equals("")) {
                            show_dialog("Name can\'t be blank because old value isn\'t null ...");
                            name.setError("Error ...");
                            name.setText(old_name);
                        }
                        else{
                            Intent resultIntent = new Intent();

                            byte[] the_recording = null;

                            if (full_recording != null) the_recording = short2byte(full_recording);

                            resultIntent.putExtra("audio", the_recording);
                            resultIntent.putExtra("name", new_name);
                            resultIntent.putExtra("description", new_description);
                            resultIntent.putExtra("current_num_photos", "" + current_n_photos);
                            int i = 0;
                            for (Photo p : photos) {
                                resultIntent.putExtra("photo" + i, p.get_Image());
                                i++;
                            }

                            // TODO Add extras or a data URI to this intent as appropriate.
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    Photo my_photo = new Photo("photo " + current_n_photos, stream.toByteArray());
                    photos.add(my_photo);
                    current_n_photos++;
                    num_photos.setText(current_n_photos + "/3", TextView.BufferType.EDITABLE);
                    show_dialog("Photo added successfully ...");
                    break;
                case 2:
                    if (data != null) {
                        ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        name.setText(result.get(0));
                    }
                    name_speech.setImageResource(R.drawable.m_off_button);
                    break;
                case 3:
                    if (data != null) {
                        ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        String current_text = description.getText().toString();
                        description.setText(current_text + " " + result.get(0));
                    }
                    description_speech.setImageResource(R.drawable.m_off_button);
                    break;
            }
        }
        else{
            switch (requestCode) {
                case 1:
                    show_dialog("Taking photo event canceled ...");
                    break;
                case 2:
                    name_speech.setImageResource(R.drawable.m_off_button);
                    description_speech.setImageResource(R.drawable.m_off_button);
                    show_dialog("Speech recognition canceled ...");
                    break;
            }
        }
    }

    // EXTRA

    public String remove_spaces(String res){
        if(!res.equals("")) {
            StringBuilder sb = new StringBuilder();
            StringTokenizer tokenizer = new StringTokenizer(res," \n\t");
            while(tokenizer.hasMoreTokens()){
                sb.append(tokenizer.nextToken()+" ");
            }
            return sb.toString();
            /*
            int i;
            for (i = 0; i < res.length() && (res.charAt(i) == ' ' || res.charAt(i) == '\n' || res.charAt(i) == '\t'); i++);
            return res.substring(i);*/
        }
        return res;
    }

    public void show_dialog(String str){
        Toast alert = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        alert.show();
        DORA.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }

    // SPEECH INPUT
    private void promptSpeechInput(int box) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.UK);
        try {
            startActivityForResult(intent, box);
        } catch (ActivityNotFoundException a) {
            show_dialog("Sorry, but your device doesn\'t support speech input ...");
        }
    }

    // PLAY RECORDING
    private void PlayAudio(short[] data) throws IOException{
        if (data==null){
            show_dialog("No recorded file to play ...");
            return;
        }

        AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, RECORDER_SAMPLE, AudioFormat.CHANNEL_OUT_DEFAULT,
                AudioFormat.ENCODING_PCM_16BIT, BUFFERSIZE, AudioTrack.MODE_STREAM);


        if (at==null){
            show_dialog("Can\'t play recorded file ...");
            return;
        }

        int size = data.length;
        at.play();
        at.write(data, 0, size);

        at.stop();
        at.release();
    }

    // RECORDING METHODS
    private void writeAudioDataToFile() {
        short sData[] = new short[BUFFERSIZE];
        int read_bytes;

        while (recording) {
            read_bytes = recorder.read(sData, 0, BUFFERSIZE);
            if(full_recording == null){
                full_recording = new short[read_bytes];
                full_recording = Arrays.copyOf(sData, read_bytes);
            }
            else{
                short[] destination = new short[full_recording.length + read_bytes];
                System.arraycopy(full_recording, 0, destination, 0, full_recording.length);
                System.arraycopy(sData, 0, destination, full_recording.length, read_bytes);

                full_recording = new short[destination.length];
                full_recording = Arrays.copyOf(destination, destination.length);
            }
        }

    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }
}