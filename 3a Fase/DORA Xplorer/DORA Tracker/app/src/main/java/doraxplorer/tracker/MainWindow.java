package doraxplorer.tracker;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import business.Photo;
import business.UserPoint;

public class MainWindow extends Activity {

    private TextToSpeech DORA;
    private EditText reference;
    private Button ok_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_window);

        reference = (EditText) findViewById(R.id.reference);
        ok_button = (Button) findViewById(R.id.ok_button);

        DORA  = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    DORA.setLanguage(Locale.UK);
                    DORA.setSpeechRate(0.7f);
                    show_dialog("DORA Assistant is ready ...");
                }
                else{
                    show_dialog("Error starting DORA Assistant ...");
                }
            }
        });

        ok_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String my_reference = "/DORA/"+reference.getText().toString();

                File extStore = Environment.getExternalStorageDirectory();
                File myFile = new File(extStore.getAbsolutePath() + my_reference + "/ManagerReport.xml");

                if(myFile.exists() && !reference.equals("")){
                    show_dialog("File found ...");
                    Intent myIntent = new Intent( MainWindow.this, MenuWindow.class);
                    myIntent.putExtra("reference", extStore.getAbsolutePath() + my_reference);
                    startActivityForResult(myIntent,1);
                }
                else {
                    reference.setError("Error ...");
                    if(reference.getText().toString().equals("")){
                        show_dialog("Filename can't be blank ...");
                    }
                    else {
                        show_dialog("File not found ...");
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                switch(resultCode) {
                    case 1:
                        // JUST GOT BACK FROM MENU TO MAKE NEW ROUTE
                        break;
                    case 2:
                        // JUST GOT BACK FROM MENU TO EXIT
                        finish();
                        break;

                }
                break;
        }
    }

    public void show_dialog(String str){
        Toast alert = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        alert.show();
        DORA.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }

}
