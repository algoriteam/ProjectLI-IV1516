package business;

public class Photo {

    // VARIAVEIS
    private String name;
    private byte[] image;

    // CONSTRUTORES
    public Photo(String n, byte[] i){
        name = n;
        image = i;
    }

    // GETS / SETS
    public String get_Name(){
        return name;
    }

    public void set_Name(String a){
        name = a;
    }

    public byte[] get_Image(){
        return image;
    }

    public void set_Image(byte[] a){
        image = a;
    }

    public boolean equals(Object o){
        if (this == o) return true;
        if ((o == null) || (!this.getClass().getName().equals(o.getClass().getName()))) return false;
        else{
            Photo aux = (Photo)o;
            return aux.name.equals(name) && aux.image.equals(image);
        }
    }
}
