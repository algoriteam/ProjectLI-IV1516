package business;

public class Point {

    // VARIAVEIS
    private double latitude;
    private double longitude;

    // CONSTRUTORES
    public Point()
    {
        latitude = longitude = 0.0;
    }

    public Point(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Point(Point c){
        this.latitude = c.latitude;
        this.longitude = c.longitude;
    }

    // GETS / SETS
    public double get_Latitude()
    {
        return latitude;
    }

    public void set_Latitude(double a)
    {
        latitude = a;
    }

    public double get_Longitude()
    {
        return longitude;
    }

    public void set_Longitude(double a)
    {
        longitude = a;
    }

    // OUTROS METODOS
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if ((o == null) || (!this.getClass().getName().equals(o.getClass().getName())))
            return false;
        else
        {
            Point aux = (Point)o;

            return latitude == aux.latitude && longitude == aux.longitude;
        }
    }

    private static double RADIUS = 6371; // Raio da Terra

    private double Haversine(double teta)
    {
        return ((1 - Math.cos(teta)) * (Math.pow(2, -1)));
    }

    public double Distance(Point p)
    {
        double lat1 = p.latitude;
        double long1 = p.longitude;

        return 2 * RADIUS * Math.asin(Math.sqrt(Haversine(ToRadians(lat1) - ToRadians(latitude))
                + Math.cos(ToRadians(latitude)) * Math.cos(ToRadians(lat1))
                * Haversine(ToRadians(long1) - ToRadians(longitude))));
    }

    public double ToRadians(double angle)
    {
        return (Math.PI / 180) * angle;
    }
}
