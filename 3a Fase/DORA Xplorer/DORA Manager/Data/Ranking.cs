﻿using System;
using System.IO;

namespace Data
{
    public enum Ranking
    {
        [IODescription("Beginner")]
        Beginner = 1,
        [IODescription("Rookie")]
        Rookie = 2,
        [IODescription("Navigator")]
        Navigator = 3,
        [IODescription("Adventurer")]
        Adventurer = 4,
        [IODescription("Explorer")]
        Explorer = 5,
        [IODescription("DORA's Sidekick")]
        Sidekick = 6
    }

    public class Ranking_Solver
    {
        public static Ranking FromDescription(string description)
        {
            switch (description)
            {
                case "Beginner":
                    return Ranking.Beginner;
                case "Rookie":
                    return Ranking.Rookie;
                case "Navigator":
                    return Ranking.Navigator;
                case "Adventurer":
                    return Ranking.Adventurer;
                case "Explorer":
                    return Ranking.Explorer;
                case "DORA's Sidekick":
                    return Ranking.Sidekick;
                default:
                    return Ranking.Beginner;
            }
        }

        public static String FromEnum(Ranking description)
        {
            return Enum.GetName(typeof(Ranking), (Ranking) description);
        }
    }
}
