﻿using Business;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;

namespace Data.DAO
{
    public class ActivityPointDAO : ISet<ActivityPoint>
    {
        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Utilizador_Pontos.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Ponto_Sessaos.IsReadOnly;
                }
                return aux;
            }
        }

        public bool Add(ActivityPoint item)
        {
            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var points = from point
                             in db.Ponto_Sessaos
                             where point.Sessaocodigo.Equals(item.Reference) &&
                                   point.Pontolatitude.Equals(item.Latitude) &&
                                   point.Pontolongitude.Equals(item.Longitude) &&
                                   point.ordem == item.Order
                             select point;

                if (points.Any())
                    return false;
                else
                {
                    Ponto_Sessao aux = new Ponto_Sessao();

                    aux.Sessaocodigo = item.Reference;
                    aux.Pontolatitude = item.Latitude;
                    aux.Pontolongitude = item.Longitude;
                    aux.ordem = item.Order;

                    db.Ponto_Sessaos.InsertOnSubmit(aux);
                    db.SubmitChanges();
                    return true;
                }
            }
        }

        public void Clear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Ponto_Sessao");
            }
        }

        public bool Contains(ActivityPoint item)
        {
            bool exists;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Ponto_Sessaos
                            where point.Sessaocodigo.Equals(item.Reference) &&
                                  point.Pontolatitude.Equals(item.Latitude) &&
                                  point.Pontolongitude.Equals(item.Longitude) &&
                                  point.ordem == item.Order
                            select point;

                if (users.Any())
                {
                    Ponto_Sessao aux = users.First();

                    if (aux.Equals(item))
                        exists = true;
                    else
                        exists = false;
                }
                else
                    exists = false;
            }
            return exists;
        }

        public void CopyTo(ActivityPoint[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Array is null");
            else if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("Array Index is less than 0");
            else if ((array.Length - arrayIndex) < this.Count)
                throw new ArgumentException("The number of elements in the source is greater" +
                    "than the available space from arrayIndex to the end of the destination array");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Ponto_Sessaos select user;

                    foreach (Ponto_Sessao aux in users)
                        array[arrayIndex++] = new ActivityPoint(new Point(aux.Pontolatitude, aux.Pontolongitude), 
                            aux.Sessaocodigo, aux.ordem.Value);
                }
            }
        }

        public void ExceptWith(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<ActivityPoint> GetEnumerator()
        {
            IEnumerator<ActivityPoint> res;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                List<ActivityPoint> list = new List<ActivityPoint>();
                var users = from user in db.Ponto_Sessaos select user;

                foreach (Ponto_Sessao aux in users)
                    list.Add(new ActivityPoint(new Point(aux.Pontolatitude, aux.Pontolongitude),
                            aux.Sessaocodigo, aux.ordem.Value));

                res = list.GetEnumerator();
            }
            return res;
        }

        public void IntersectWith(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool IsProperSubsetOf(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool IsProperSupersetOf(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool IsSubsetOf(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool IsSupersetOf(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool Overlaps(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public bool Remove(ActivityPoint item)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Ponto_Sessaos
                            where point.Sessaocodigo.Equals(item.Reference) &&
                                  point.Pontolatitude.Equals(item.Latitude) &&
                                  point.Pontolongitude.Equals(item.Longitude) &&
                                  point.ordem == item.Order
                            select point;

                if (!users.Any())
                    aux = false;
                else
                {
                    foreach (Ponto_Sessao user in users)
                        db.Ponto_Sessaos.DeleteOnSubmit(user);
                    db.SubmitChanges();
                    aux = true;
                }
            }
            return aux;
        }

        public bool SetEquals(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public void SymmetricExceptWith(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        public void UnionWith(IEnumerable<ActivityPoint> other)
        {
            throw new NotImplementedException();
        }

        void ICollection<ActivityPoint>.Add(ActivityPoint item)
        {
            this.Add(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
