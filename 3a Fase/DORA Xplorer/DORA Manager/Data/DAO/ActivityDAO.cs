using Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Data.DAO
{
    public class ActivityDAO : IDictionary<string, Activity>
    {
        public Activity this[string key]
        {
            get
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else
                {
                    Sessao aux = new Sessao();

                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var activitys = from activity in db.Sessaos where activity.codigo.Equals(key) select activity;

                        if (!activitys.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                            aux = activitys.First();
                    }
                    return new Activity(aux.codigo, aux.pontuacao, aux.data.Value, 
                        (aux.registo_de_voz != null ? aux.registo_de_voz.ToArray() : null)
                        , aux.relatorio, aux.Utilizadoremail, aux.distancia);
                }
            }

            set
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else if (this.IsReadOnly == true)
                    throw new NotSupportedException("The Table is read-only");
                else
                {
                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var activitys = from activity in db.Sessaos where activity.codigo.Equals(key) select activity;

                        if (!activitys.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                        {
                            foreach (Sessao aux in activitys)
                            {
                                aux.codigo = key;
                                aux.pontuacao = value.Experience;
                                aux.data = value.Date;

                                if (value.Voice_register != null)
                                    aux.registo_de_voz = new System.Data.Linq.Binary(value.Voice_register);
                                else
                                    aux.registo_de_voz = null;

                                aux.relatorio = value.Report;
                                aux.Utilizadoremail = value.Email;
                                aux.distancia = value.Distance;
                            }
                            db.SubmitChanges();
                        }
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Sessaos.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Sessaos.IsReadOnly;
                }
                return aux;
            }
        }

        public ICollection<string> Keys
        {
            get
            {
                List<string> emails = new List<string>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var activitys = from activity in db.Sessaos select activity.codigo;
                    foreach (string codigo in activitys)
                        emails.Add(codigo);
                }
                return emails;
            }
        }

        public ICollection<Activity> Values
        {
            get
            {
                List<Activity> activitys = new List<Activity>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var activity = from aux in db.Sessaos select aux;
                    foreach (Sessao aux in activity)
                                activitys.Add(new Activity(aux.codigo, aux.pontuacao, aux.data.Value,
                                (aux.registo_de_voz != null ? aux.registo_de_voz.ToArray() : null)
                                , aux.relatorio, aux.Utilizadoremail, aux.distancia));
                }
                return activitys;
            }
        }

        public void Add(KeyValuePair<string, Activity> item)
        {
            this.Add(item.Key, item.Value);
        }

        public void Add(string key, Activity value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else if (this.IsReadOnly == true)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var activitys = from activity in db.Sessaos where activity.codigo.Equals(key) select activity;

                    if (activitys.Any())
                        throw new ArgumentException("An Element with the same Key already exists");
                    else
                    {
                        Sessao aux = new Sessao();
                        aux.codigo = key;
                        aux.pontuacao = value.Experience;
                        aux.data = value.Date;

                        if (value.Voice_register != null)
                            aux.registo_de_voz = new System.Data.Linq.Binary(value.Voice_register);
                        else
                            aux.registo_de_voz = null;

                        aux.relatorio = value.Report;
                        aux.Utilizadoremail = value.Email;
                        aux.distancia = value.Distance;

                        db.Sessaos.InsertOnSubmit(aux);
                        db.SubmitChanges();
                    }
                }
            }
        }

        public void Clear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Sessao");
            }
        }

        public bool Contains(KeyValuePair<string, Activity> item)
        {
            bool exists;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var activitys = from activity in db.Sessaos where activity.codigo.Equals(item.Key) select activity;

                if (activitys.Any())
                {
                    Sessao aux = activitys.First();
                    Activity act = new Activity(aux.codigo, aux.pontuacao, aux.data.Value,
                                (aux.registo_de_voz != null ? aux.registo_de_voz.ToArray() : null)
                                , aux.relatorio, aux.Utilizadoremail, aux.distancia);

                    if (act.Equals(item.Value))
                        exists = true;
                    else
                        exists = false;
                }
                else
                    exists = false;
            }
            return exists;
        }

        public bool ContainsKey(string key)
        {
            bool exists;

            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var activitys = from activity in db.Sessaos where activity.codigo.Equals(key) select activity;

                    if (activitys.Any())
                        exists = true;
                    else
                        exists = false;
                }
            }
            return exists;
        }

        public void CopyTo(KeyValuePair<string, Activity>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Array is null");
            else if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("Array Index is less than 0");
            else if ((array.Length - arrayIndex) < this.Count)
                throw new ArgumentException("The number of elements in the source is greater" +
                    "than the available space from arrayIndex to the end of the destination array");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var activitys = from activity in db.Sessaos select activity;

                    foreach (Sessao aux in activitys)
                        array[arrayIndex++] = 
                            new KeyValuePair<string, Activity>(aux.codigo, 
                                new Activity(aux.codigo, aux.pontuacao, aux.data.Value,
                                (aux.registo_de_voz != null ? aux.registo_de_voz.ToArray() : null)
                                , aux.relatorio, aux.Utilizadoremail, aux.distancia));
                }
            }
        }

        public IEnumerator<KeyValuePair<string, Activity>> GetEnumerator()
        {
            IEnumerator<KeyValuePair<string, Activity>> res;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                List<KeyValuePair<string, Activity>> aux = new List<KeyValuePair<string, Activity>>();
                var activitys = from activity in db.Sessaos select activity;

                foreach (Sessao activity in activitys)
                    aux.Add(new KeyValuePair<string, Activity>(activity.codigo,
                                new Activity(activity.codigo, activity.pontuacao, activity.data.Value,
                                (activity.registo_de_voz != null ? activity.registo_de_voz.ToArray() : null)
                                , activity.relatorio, activity.Utilizadoremail, activity.distancia)));

                res = aux.GetEnumerator();
            }
            return res;
        }

        public bool Remove(KeyValuePair<string, Activity> item)
        {
            return this.Remove(item.Key);
        }

        public bool Remove(string key)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var activitys = from activity in db.Sessaos where activity.codigo.Equals(key) select activity;

                if (!activitys.Any())
                    aux = false;
                else
                {
                    foreach (Sessao activity in activitys)
                        db.Sessaos.DeleteOnSubmit(activity);
                    db.SubmitChanges();
                    aux = true;
                }
            }
            return aux;
        }

        public bool TryGetValue(string key, out Activity value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                bool aux;

                try
                {
                    value = this[key];
                    aux = true;
                }
                catch (KeyNotFoundException e)
                {
                    value = new Activity();
                    aux = false;
                }
                return aux;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
