using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Business
{
    [Serializable]
    public class Photo
    {
        private string name;
        private ImageSource image;

        public Photo(string name, byte[] image)
        {
            Name = name;
            Image = ByteToImage(image);
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData != null)
            {
                ImageSource imgSrc;

                try
                {
                    BitmapImage biImg = new BitmapImage();
                    MemoryStream ms = new MemoryStream(imageData);
                    biImg.BeginInit();
                    biImg.StreamSource = ms;
                    biImg.EndInit();

                    imgSrc = biImg as ImageSource;
                }
                catch (Exception) { imgSrc = null; }

                return imgSrc;
            }
            else
                return null;
        }

        public Photo(string name, ImageSource image)
        {
            Name = name;
            Image = image;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public ImageSource Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return true;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                Photo aux = (Photo)o;

                return aux.Name.Equals(name) &&
                    aux.Image.Equals(Image);
            }
        }
    }
}
