﻿using Data;
using System;
using System.IO;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Business
{
    public class User
    {
        private string name, email, facebook, phone, password, street, locality;
        private DateTime birthday;
        private Ranking ranking;
        private int experience;
        private ImageSource image;
        private bool logged_in;
        private double latitude, longitude;

        public User()
        {
            this.Name = "";
            this.Email = "";
            this.Facebook = "";
            this.Phone = "";
            this.Password = "";
            this.Birthday = new DateTime();
            this.Image = null;
            this.Ranking = Ranking.Beginner;
            this.Experience = 0;
            this.Street = "";
            this.Locality = "";
            this.Logged_in = false;
            this.Latitude = 0.0;
            this.Longitude = 0.0;
        }

        public User(string name, string email, string facebook, string phone, string password,
            DateTime birthday, ImageSource image, Ranking ranking, int experience, string street, string locality, bool logged_in, 
            double latitude, double longitude)
        {
            this.Name = name;
            this.Email = email;
            this.Facebook = facebook;
            this.Phone = phone;
            this.Password = password;
            this.Birthday = birthday;
            this.image = image;
            this.Ranking = ranking;
            this.Experience = experience;
            this.Street = street;
            this.Locality = locality;
            this.Logged_in = logged_in;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public User(string name, string email, string facebook, string phone, string password,
            DateTime birthday, Ranking ranking, byte[] image, int experience, string street, string locality, bool logged_in, 
            double latitude, double longitude)
        {
            this.Name = name;
            this.Email = email;
            this.Facebook = facebook;
            this.Phone = phone;
            this.Password = password;
            this.Birthday = birthday;
            this.Image = ByteToImage(image);
            this.Ranking = ranking;
            this.Experience = experience;
            this.Street = street;
            this.Locality = locality;
            this.Logged_in = logged_in;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData != null)
            {
                ImageSource imgSrc;

                try
                {
                    BitmapImage biImg = new BitmapImage();
                    MemoryStream ms = new MemoryStream(imageData);
                    biImg.BeginInit();
                    biImg.StreamSource = ms;
                    biImg.EndInit();

                    imgSrc = biImg as ImageSource;
                }
                catch(Exception) { imgSrc = null; }

                return imgSrc;
            }
            else
                return null;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Facebook
        {
            get
            {
                return facebook;
            }

            set
            {
                facebook = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }

            set
            {
                phone = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string Street
        {
            get
            {
                return street;
            }

            set
            {
                street = value;
            }
        }

        public string Locality
        {
            get
            {
                return locality;
            }

            set
            {
                locality = value;
            }
        }

        public DateTime Birthday
        {
            get
            {
                return birthday;
            }

            set
            {
                birthday = value;
            }
        }

        public Ranking Ranking
        {
            get
            {
                return ranking;
            }

            set
            {
                ranking = value;
            }
        }

        public int Experience
        {
            get
            {
                return experience;
            }

            set
            {
                experience = value;
            }
        }

        public ImageSource Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        public bool Logged_in
        {
            get
            {
                return logged_in;
            }

            set
            {
                logged_in = value;
            }
        }

        public double Latitude
        {
            get
            {
                return latitude;
            }

            set
            {
                latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return longitude;
            }

            set
            {
                longitude = value;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return false;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                User aux = (User)o;

                return this.name.Equals(aux.Name) &&
                       this.email.Equals(aux.Email) &&
                       this.facebook.Equals(aux.Facebook) &&
                       this.phone.Equals(aux.Phone) &&
                       this.password.Equals(aux.Password) &&
                       this.street.Equals(aux.Street) &&
                       this.birthday.Equals(aux.Birthday) &&
                       this.ranking.Equals(aux.Ranking) &&
                       this.experience == aux.Experience &&
                       this.image.Equals(aux.Image) &&
                       this.logged_in == aux.Logged_in &&
                       this.latitude == aux.Latitude &&
                       this.longitude == aux.Longitude;
            }
        }

        public override string ToString()
        {
            StringBuilder aux = new StringBuilder();

            aux.Append("Email: ");
            aux.Append(email);
            aux.Append("\nName: ");
            aux.Append(name);
            aux.Append("\nRank: ");
            aux.Append(Enum.GetName(typeof(Ranking), ranking));
            aux.Append("\nPoints: ");
            aux.Append(experience);

            return aux.ToString();
        }
    }
}
