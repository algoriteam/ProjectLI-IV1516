﻿namespace Business
{
    public class ActivityPoint : Point
    {
        private string reference;
        private int order;

        public ActivityPoint() : base(0.0, 0.0)
        {
            Reference = "";
        }

        public ActivityPoint(Point p, string rf, int order) : base(p.Latitude, p.Longitude)
        {
            Reference = rf;
            Order = order;
        }

        public Point Coordinates
        {
            get
            {
                return new Point(Latitude, Longitude);
            }

            set
            {
                Latitude = value.Latitude;
                Longitude = value.Longitude;
            }
        }

        public string Reference
        {
            get
            {
                return reference;
            }

            set
            {
                reference = value;
            }
        }

        public int Order
        {
            get
            {
                return order;
            }

            set
            {
                order = value;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return true;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                ActivityPoint aux = (ActivityPoint) o;

                return base.Equals(aux) &&
                    Reference.Equals(aux.Reference) &&
                    Order == aux.Order;
            }
        }
    }
}
