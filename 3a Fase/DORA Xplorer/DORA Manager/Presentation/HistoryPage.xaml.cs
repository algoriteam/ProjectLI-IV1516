﻿using Business;
using System.IO;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System;
using Data.DAO;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for ProfilePage.xaml
    /// </summary>
    public partial class HistoryPage : Page
    {
        bool playing = false;
        private Business.DORA_Manager my_man;
        private User my_user;

        private MapPage my_map;
        private List<Activity> lista_atividades;

        public HistoryPage(Business.DORA_Manager man, User user)
        {
            InitializeComponent();
            my_man = man;
            my_user = user;
            lista_atividades = new List<Activity>();

            my_map = new MapPage();
            my_map.Resize(526, 302);

            InsideFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            InsideFrame.Navigate(my_map);

            // GET ACTIVITY LIST

            // CONSTRUCT COMBOBOX
            References_Load();
        }

        private void References_Load()
        {
            // ... A List.
            lista_atividades = my_man.Get_Activities_Of_User(my_user.Email);
            List<string> data = new List<string>();

            foreach (Activity a in lista_atividades)
                data.Add(a.Reference);
            
            // ... Assign the ItemsSource to the List.
            References.ItemsSource = data;

            // ... Make the first item selected.
            // References.SelectedIndex = 0;
        }

        private void References_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Activity aux = new Activity();

            if (References.SelectedIndex > -1)
            {
                string rf = References.SelectedItem as string;

                foreach (Activity at in lista_atividades)
                {
                    if (at.Reference.Equals(rf))
                    {
                        aux = at;
                        break;
                    }
                }

                if (aux.Voice_register != null)
                {
                    PlayStop.Visibility = Visibility.Visible;
                }
                else
                {
                    PlayStop.Visibility = Visibility.Hidden;
                }

                if (!my_man.Is_Finished(aux))
                {
                    Remove_Button.Visibility = Visibility.Visible;
                }
                else
                {
                    Remove_Button.Visibility = Visibility.Hidden;
                }

                Begin.Foreground = new SolidColorBrush(Colors.Black);
                End.Foreground = new SolidColorBrush(Colors.Black);

                // BEGIN
                Business.Point a = aux.Points[0].Coordinates;
                Begin.Text = my_map.Geocode(a.Latitude,a.Longitude);

                // END
                int last_index = aux.Points.Count;
                Business.Point b = aux.Points[last_index - 1];
                End.Text = my_map.Geocode(b.Latitude, b.Longitude);

                // EXPERIENCE POINTS
                Points.Content = aux.Experience;

                // DATE FORMAT
                if (my_man.Is_Finished(aux))
                    Date.Content = aux.Date.ToString("yyyy/MM/dd");
                else
                    Date.Content = "Not Completed";
            }
            else
            {
                Begin.Foreground = new SolidColorBrush(Colors.Gray);
                End.Foreground = new SolidColorBrush(Colors.Gray);
                Begin.Text = "Begin";
                End.Text = "End";
            }
        }

        private void Mail_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (References.SelectedIndex > -1)
            {
                Activity aux = my_man.Get_Activity(References.SelectedItem as string);

                // SEND EMAIL
                if (my_man.Is_Finished(aux))
                    new EmailWindow(my_user.Email, my_man, aux.Reference);
                else
                    MessageBox.Show("You can only share your activity by email when you complete it");
            }
        }

        private void Info_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            my_map = new MapPage();
            my_map.Resize(526, 302);
            InsideFrame.Navigate(my_map);

            Activity aux = new Activity();

            if (References.SelectedIndex > -1)
            {
                string rf = References.SelectedItem as string;

                foreach (Activity at in lista_atividades)
                {
                    if (at.Reference.Equals(rf))
                    {
                        aux = at;
                        break;
                    }
                }

                int i = 1;
                aux.SortPoints(new UserPointComparator(rf));
                foreach (UserPoint u in aux.Points)
                {
                    my_map.Route_Point(new Business.Point(u.Coordinates.Latitude, u.Coordinates.Longitude), "[Coordinates]\n >Latitude: " + u.Coordinates.Latitude + "\n >Longitude: " + u.Coordinates.Longitude + "\n[User Input]\n >Name: " + u.Name + "\n >Description: " + u.Description, i++);
                }
            }
        }

        private void Remove_Button_Click(object sender, RoutedEventArgs e)
        {
            if (References.SelectedIndex > -1)
            {
                Activity aux = my_man.Get_Activity(References.SelectedItem as string);

                my_man.Remove_Activity(aux.Reference, my_user.Email);
                MessageBox.Show("Activity removed with sucess!");

                Date.Content = "NULL";
                Begin.Text = "Begin";
                End.Text = "End";
                Begin.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
                End.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));

                References_Load();

                PlayStop.Visibility = Visibility.Hidden;
                Remove_Button.Visibility = Visibility.Hidden;
            }
        }

        private void PlayStop_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (References.SelectedIndex > -1)
            {
                Activity aux = my_man.Get_Activity(References.SelectedItem as string);

                WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

                wplayer.URL = "voice.wav";

                if (File.Exists("voice.wav"))
                    File.Delete("voice.wav");

                if (aux.Voice_register != null)
                {
                    if (!playing)
                    {
                        File.WriteAllBytes("voice.wav", aux.Voice_register);
                        wplayer.controls.play();
                        PlayStop.Source = new BitmapImage(new Uri("/Resources/STOP.png", UriKind.Relative));
                        playing = true;
                    }
                    else
                    {
                        wplayer.controls.stop();
                        PlayStop.Source = new BitmapImage(new Uri("/Resources/PLAY.png", UriKind.Relative));
                        playing = false;
                        File.Delete("voice.wav");
                    }
                }
                else PlayStop.Source = new BitmapImage(new Uri("/Resources/MICROPHONE.png", UriKind.Relative));
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            File.Delete("voice.mp3");
        }
    }

    public class UserPointComparator : IComparer<UserPoint>
    {
        private string reference;

        public UserPointComparator(string rf)
        {
            reference = rf;
        }

        public int Compare(UserPoint x, UserPoint y)
        {
            int ord_x = 0, ord_y = 0;
            ActivityPointDAO act_pts = new ActivityPointDAO();

            foreach (ActivityPoint pt in act_pts)
            {
                if (pt.Coordinates.Equals(x.Coordinates) && pt.Reference.Equals(reference))
                    ord_x = pt.Order;
                else if (pt.Coordinates.Equals(y.Coordinates) && pt.Reference.Equals(reference))
                    ord_y = pt.Order;
            }

            if (ord_x < ord_y)
                return -1;
            else if (ord_x == ord_y)
                return 0;
            else
                return 1;
        }
    }
}
