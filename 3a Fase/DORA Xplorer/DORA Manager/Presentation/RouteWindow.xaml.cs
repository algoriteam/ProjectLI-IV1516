﻿using Business;
using DORA_Manager.Presentation;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RouteWindow : MetroWindow
    {
        Business.DORA_Manager my_man;
        MapPage my_map;
        MenuWindow menu;

        Business.Point first;
        Business.Point second;
        double distance;
        User my_user;

        int pin;

        List<Business.Point> points;

        public RouteWindow(MenuWindow m, Business.DORA_Manager man, User u)
        {
            InitializeComponent();
            menu = m;
            my_man = man;
            my_map = new MapPage();
            distance = 0.0;
            pin = 0;
            points = new List<Business.Point>();
            my_user = u;
        }

        public void first_point(Business.Point a)
        {
            pin++;
            Begin.Text = my_map.Geocode(a.Latitude,a.Longitude);
            Pin_Number.Content = "" + pin;
            second = new Business.Point(a.Latitude, a.Longitude);
            points.Add(second);
        }

        public void new_point(Business.Point a)
        {
            pin++;
            End.Text = my_map.Geocode(a.Latitude, a.Longitude);

            first = new Business.Point(second.Latitude, second.Longitude);
            second = new Business.Point(a.Latitude, a.Longitude);
            points.Add(second);
            distance += first.Distance(second);

            Distance.Content = string.Format("{0:0.00}", distance);
            Pin_Number.Content = "" + pin;
        }

        private void Begin_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Begin.Text.Equals("Begin"))
            {
                Begin.Text = "";
                Begin.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Begin_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Begin.Text.Equals(""))
            {
                Begin.Text = "Begin";
                Begin.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void End_GotFocus(object sender, RoutedEventArgs e)
        {
            if (End.Text.Equals("End"))
            {
                End.Text = "";
                End.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void End_LostFocus(object sender, RoutedEventArgs e)
        {
            if (End.Text.Equals(""))
            {
                End.Text = "End";
                End.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            string rf = my_man.Create_Activity(my_user.Email, points, distance).Reference;
            MessageBox.Show("Little DORA created your new activity successfully\n\n Your Reference is: " + rf);
            new MenuWindow(my_man, my_user);
            menu.Close();
            Close();
        }
    }
}
