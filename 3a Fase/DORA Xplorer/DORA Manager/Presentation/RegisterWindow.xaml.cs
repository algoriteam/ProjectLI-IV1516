﻿using Business;
using DORA_Manager.Presentation;
using MahApps.Metro.Controls;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RegisterWindow : MetroWindow
    {
        Business.DORA_Manager my_man;

        Photo avatar;
        string username;
        string password;

        DateTime birthday;
        string name;
        string telephone;
        string facebook;
        string street;
        string local;

        private MapPage my_map;
        private Location my_house;

        public RegisterWindow(Business.DORA_Manager man)
        {
            InitializeComponent();
            my_man = man;
            username = "";
            password = "";
            birthday = new DateTime();
            name = "";
            telephone = "";
            facebook = "";
            street = "";
            local = "";
            my_map = new MapPage();
            my_map.Resize(478, 249);
            Birth.DisplayMode = CalendarMode.Year;

            avatar = new Photo("", image.Source);

            my_house = new Location();

            InsideFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            InsideFrame.Navigate(my_map);
        }

        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            Close();
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            street = Street.Text;
            local = Local.Text;
            if (!street.Equals("") || !local.Equals(""))
            {
                my_map.first_location = false;
                GeoCoordinate aux = my_map.Geocode(street + " " + local);

                my_house = new Location(aux.Latitude, aux.Longitude);
                my_map.Push_House(my_house, "[Address]\n >Street: " + street + "\n >Locality: " + local, username, false);
                my_map.Center_Map(my_house, 17.0);
            }
        }

        private void Username_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals("Username"))
            {
                Username.Text = "";
                Username.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Username_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals(""))
            {
                Username.Text = "Username";
                Username.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Password_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Password.Password.Equals("Password"))
            {
                Password.Password = "";
                Password.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Password_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Password.Password.Equals(""))
            {
                Password.Password = "Password";
                Password.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Name_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Name.Text.Equals("Name"))
            {
                Name.Text = "";
                Name.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Name_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Name.Text.Equals(""))
            {
                Name.Text = "Name";
                Name.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Telephone_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Telephone.Text.Equals("Telephone"))
            {
                Telephone.Text = "";
                Telephone.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Telephone_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Telephone.Text.Equals(""))
            {
                Telephone.Text = "Telephone";
                Telephone.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Facebook_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Facebook.Text.Equals("Facebook"))
            {
                Facebook.Text = "";
                Facebook.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Facebook_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Facebook.Text.Equals(""))
            {
                Facebook.Text = "Facebook";
                Facebook.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Street_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Street.Text.Equals("Street"))
            {
                Street.Text = "";
                Street.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Street_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Street.Text.Equals(""))
            {
                Street.Text = "Street";
                Street.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Local_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Local.Text.Equals("Local"))
            {
                Local.Text = "";
                Local.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Local_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Local.Text.Equals(""))
            {
                Local.Text = "Local";
                Local.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            avatar = my_man.Load_Image();

            if (avatar != null)
            {
                Photo.Source = avatar.Image;
                Image.Text = avatar.Name;
                image.Source = avatar.Image;
            }
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            try {
                birthday = Birth.SelectedDate.Value;
            }
            catch(InvalidOperationException)
            {
                birthday = DateTime.Now;
            }

            username = Username.Text;
            password = Password.Password;
            name = Name.Text;
            telephone = Telephone.Text;
            facebook = Facebook.Text;
            street = Street.Text;
            local = Local.Text;

            if (birthday.Year < DateTime.Now.Year && !username.Equals("Username") && !password.Equals("Password") && !name.Equals("Name") && !street.Equals("Street") && !local.Equals("Local"))
            {
                // Get latitude and longitude just to be sure
                GeoCoordinate aux = my_map.Geocode(street + " " + local);
                my_house = new Location(aux.Latitude, aux.Longitude);

                if (my_man.Register(username, password, name, street, local, my_house.Latitude, my_house.Longitude, birthday, (avatar.Image != null ? avatar.Image : null), facebook, telephone))
                {
                    MessageBox.Show("Little DORA created your account successfully!");
                    new MainWindow().Show();
                    Close();
                }
                else MessageBox.Show("Little DORA is sad and couldn't create an account with this credentials!");
            }
            else
            {
                MessageBox.Show("Invalid inputed values!");
                foreach (TextBox tb in FindVisualChildren<TextBox>(this))
                {
                    if ((tb.Name.Equals("Username") || tb.Name.Equals("Name") || tb.Name.Equals("Street") || tb.Name.Equals("Local")) && tb.Text.Equals(tb.Name))
                    {
                        tb.BorderBrush = System.Windows.Media.Brushes.Red;
                        tb.BorderThickness = new Thickness(2,2,2,2);
                    }
                    else
                    {
                        tb.BorderBrush = System.Windows.Media.Brushes.Gray;
                        tb.BorderThickness = new Thickness(1, 1, 1, 1);
                    }
                }

                if (Password.Password.Equals("Password"))
                {
                    Password.BorderBrush = System.Windows.Media.Brushes.Red;
                    Password.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                else
                {
                    Password.BorderBrush = System.Windows.Media.Brushes.Gray;
                    Password.BorderThickness = new Thickness(1, 1, 1, 1);
                }

                if(birthday.Year >= DateTime.Now.Year)
                {
                    Birth.BorderBrush = System.Windows.Media.Brushes.Red;
                    Birth.BorderThickness = new Thickness(2, 2, 2, 2);
                }
            }
        }

        private IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}
