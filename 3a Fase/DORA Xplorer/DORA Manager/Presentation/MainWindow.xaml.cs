﻿using Business;
using MahApps.Metro.Controls;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        Business.DORA_Manager man = new Business.DORA_Manager();

        public MainWindow()
        {
            InitializeComponent();

            if (!CheckForInternetConnection())
            {
                MessageBox.Show("No Internet Connection Available!\n\nPlease connect to the Internet and open app again");
                Application.Current.Shutdown();
            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                Ping myPing = new Ping();
                string host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void Password_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Password.Password.Equals("Password"))
            {
                Password.Password = "";
                Password.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Password_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Password.Password.Equals(""))
            {
                Password.Password = "Password";
                Password.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Username_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals("Username"))
            {
                Username.Text = "";
                Username.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Username_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals(""))
            {
                Username.Text = "Username";
                Username.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Login_Button_Click(object sender, RoutedEventArgs e)
        {
            string mail = Username.Text;
            string pass = Password.Password;
            
            int ret = man.Login(mail, pass);
            switch (ret)
            {
                case 0:
                    User one_user = man.getUser(mail);
                    new MenuWindow(man, one_user).Show();
                    Close();
                    break;
                case 1:
                    MessageBox.Show("Invalid Username/Password!","ERROR");
                    break;
                case 2:
                    MessageBox.Show("User already logged in!", "ERROR");
                    break;
            }
        }

        private void Register_Button_Click(object sender, RoutedEventArgs e)
        {
            new RegisterWindow(man).Show();
            Close();
        }
    }
}
