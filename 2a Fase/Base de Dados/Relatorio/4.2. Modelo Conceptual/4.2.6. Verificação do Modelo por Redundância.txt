4.2.6 – Verificação do Modelo por Redundância
Neste instante examina-se o modelo conceptual de forma a encontrar qualquer tipo de redundância que possa ocorrer. Isto é, procurar casos em que, a partir de entidades ou relações diferentes, consegue-se obter a mesma informação.
Após uma análise do modelo conceptual conclui-se que este não apresenta qualquer relação redundante.
