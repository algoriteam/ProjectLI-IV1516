4.3.4. Validação das Relações a partir das Transações
Partimos agora para a validação das transações no modelo lógico que aqui pretendemos, ou seja, averiguar se os requisitos impostos se encontram satisfeitas.
De seguida apresentamos e analisamos algumas das transações mais significantes da plataforma

	<< 1-pontos.PNG >>
	Figura: Utilizador cria novo percurso.
Utilizador cria uma sessão com novo percurso (1,2,3) – é verificado se algum dos pontos já tinha sido visitado (3,4); atualiza – opcionalmente – os pontos já visitados (4); os novos pontos visitados são acrescentados a base de dados (3,4).

	<< 2-pontuacoes.PNG >>
	Figura: Consultar quadro geral de pontuações.
Consultar quadro geral de pontuações (1) – listados todos os utilizadores e as suas pontuações, ordenados pela ordem decrescente de pontos.

	<< 3-local_popular_fotografias.PNG >>
	Figura: Forografias de um local popular.
Conjunto de fotografias de um local popular – listar o conjunto de fotografias de um local muito frequentado pelos utilizadores (1).

	<< 4-Local_popular.PNG >>
	Figura: Local mais visitado.
O local mais visitado – contar numero que visitas para todos os locais, visualizando pela ordem decrescente (1).

	<< 5-utilizador_assiduo.PNG >>
	Figura: Utilizador mais "descobridor".
Quem encontrou mais locais – entre todos os utilizadores (1) visualizar o utilizador que encontrou mais pontos (2).
